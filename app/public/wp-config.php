<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Nk/tFh4tctkLG8Tz6P7m4VJzV2uO8ovUmkxW3n9n30wZdLJah8RUkkoY9P4PnrnG3AvDuSYkYTUYT8o1owFG4w==');
define('SECURE_AUTH_KEY',  'GZDwj1CWY1o/ueZ3kBTLV+ENdd/LESNdiZSrTdMi24tsljRMj/XW5K9I5yzSBTzFH8IoAhF0hUoyaUe76BNbCg==');
define('LOGGED_IN_KEY',    'KNpYq7ZmoyoHsA+7wJOlKSU9BYFv2RXU9GRPdenzpnOOo1URjHeNrIQkPgiuQYPOzP0BTSOOC635GubeR9LwiA==');
define('NONCE_KEY',        'BZICVkBbG6m+BID499D6wEKazNz4Uw6BYT0mx66xsgYDQ7H7hVNwtcxQ04Ll2g3myzHVT2Di+H8ScR/7If8wnQ==');
define('AUTH_SALT',        'wp9l0vtKpgrr9ASv1fAyvOc19B2fTNt9Nj5W3I6WDD+SS2JmRBY0hPDgTo7tTZlL2Lo6E3mJuUb9pYjGFrQFig==');
define('SECURE_AUTH_SALT', 'nflijleg6tavirwmLNJnhTffYrAcdPdD3zlCzaxZU/JMFvdKt8SNs9tT+mNgBMJ4ep7j9Tbqaq0eJckvnHWGTQ==');
define('LOGGED_IN_SALT',   '4i1L5lMvzCwG2EmZGc8kcp2XHgdzOv0pRGksEa51DqeUUfsihCRi33z12Y1CpBCzQ5JywRNYJS+OHZGtO5Yauw==');
define('NONCE_SALT',       'aa7wwU+cMNoFt8xO/ZjiqbDkGGiFbziydnaUtkj/MDHQEAdTWA5V+HZkBYWHQ7xHuSZGYBLNZKXNkJS23JIR1Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
